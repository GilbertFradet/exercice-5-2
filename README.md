# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Génération et Parsage JSON 

Notre startup a fait le buzz auprès de ses investisseurs grâce au prototype développé avec Swing. 
Le hic, c'est qu'à présent, pour avancer, nous devons accepter l'introduction d'un nouvel investisseur prenant le rôle de CTO, car ayant la confiance du BOard en matière d'industrialisation logicielle.
Ce nouveau CTO ainsi proclamé, un certain nombre de pré-requis nous sont alors imposés.

# Applications WEB, MVC, Services Rest
 ++ Mise en place d'une API Rest
 

## Contexte
* Au programme de ce cours: Services Rest avancés

Comme à son habitude, notre meilleur client revient nous voir avec un fort enthousiasme.
Il a eu vent des projets de refonte JSON, et aussi de l'éventuelle création de services Rest. IL n'a vraiment pas tout compris, mais il commence à répandre la nouvelle que notre architecture est déjà 100% Restful
EN plus de ça, il nous demande de prototyper rapidement une interface Web s'appuyant sur nos services...mais donc, il faut les développer de toute urgence !

## Objectifs
* Mises en application :
- [ ] (Exercice 1) Service REST : Lecture d'un Badge
- [x] **(Exercice 2) Service REST : suppression d'un Badge**
- [ ] (Exercice 3) Service REST : Ajout d'un Badge
- [ ] (Exercice 4) Services Rest : Génération d'un client Java et branchement de l'interface Swing en mode client/serveur

----

Afin d'implémenter le service de suppression de badge, abordons le problème par impacts, couche par couche.

## Impact sur le Modèle

```plantuml
@startuml

title __MODEL's Class Diagram__\n

  namespace fr.cnam.foad.nfa035.badges.wallet.model {
    class fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge {
        - badge : File
        - begin : Date
        - end : Date
        - serial : String
        + DigitalBadge()
        + DigitalBadge()
        + DigitalBadge()
        + compareTo()
        + equals()
        + getBadge()
        + getBegin()
        + getEnd()
        + getMetadata()
        + getSerial()
        + hashCode()
        {static} + makeDeadBadge()
        + setBadge()
        + setBegin()
        + setEnd()
        + setMetadata()
        + setSerial()
        + toString()
        - DigitalBadge()
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.wallet.model {
    class fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata {
        - badgeId : int
        - imageSize : long
        - imageType : String
        - walletPosition : long
        + DigitalBadgeMetadata()
        + DigitalBadgeMetadata()
        + DigitalBadgeMetadata()
        + compareTo()
        + equals()
        + getBadgeId()
        + getImageSize()
        + getImageType()
        + getWalletPosition()
        + hashCode()
        + setBadgeId()
        + setImageSize()
        + setImageType()
        + setWalletPosition()
        + toString()
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.wallet.model {
    class fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet {
        - allBadges : Set<DigitalBadge>
        - deadLinesPositions : Set<Long>
        - deletingBadges : Set<DigitalBadge>
        + DigitalWallet()
        + addDeletingBadge()
        + commitBadgeDeletion()
        + getAllBadges()
        + getDeadLinesPositions()
        + getDeletingBadges()
        + setAllBadges()
        + setDeadLinesPositions()
    }
  }
  

  fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge o-right- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata : metadata
  fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet o-- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge : badges
  fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet o-- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge : suppression

@enduml
```

 - [ ] Appliquez le diagramme-classe ci-dessus à votre couche modèle (donc au niveau du package portant le même nom).
   - [ ] L'objet **DigitalWallet** devient à présent le principal représentant des métadonnées du Wallet. 
     - [ ] Il contient lui-même son prédécesseur pour ce rôle, le Set de **DigitalBadge**, mais aussi 
     - [ ] un autre **Set<DigitalBadge>** permettant de regrouper les instances de badges potentiellement à supprimer.
     - [ ] Ce n'est pas tout, il contient enfin un Set<Long> avec les positions des lignes mortes une fois que la suppression est effective.

Pour résumer, nous avons codé là un objet à la fois fonctionnel puisque représentant du protefeuille de badge, et contenant lui-même nos badges, ce qui fait sens...
Et en y regardant de plus prêt notre objet porte des attributs bien techniques effectivement, afin de nous permettre de gérer la suppression de nos badges au sein d'un fichier en accès direct.
  
## Impact sur la désérialisation des Métadonnées

 - [ ] Comme le marquage d'un enregistrement n'est pas encore implémenté au niveau de la sérialisation, dans un premier codez le service de restitution des métadonnées comme si il pouvait se trouver dans le fichier json, une ligne marquée.
   - [ ] Pour cela aidez-vous de la clause **catch()** lorsqu'une IOException est "throwée"
   - [ ] Faite de même lorsque le format de base est csv.
   - [ ] Modifiez vos jeux de données de test pour vérifier la couverture du cas "ligne supprimée", ajoutant également aux tests unitaires des conditions (assertions) relatives à l'enregistrement supprimé.
   - [ ] Typiquement, le test peut être comme suit, et accepté à partir du moment ou il passe au vert et où il est d'avantage documenté (commentaires à chaque ligne d'assertion)
     - [ ] Pour le mode CSV
          ```java
              @Test
              void testGetMetadataWithDeletedRecords() throws IOException {
                  DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_deleted.csv");
                  DigitalWallet wallet = dao.getWalletMetadata();
                  Set<DigitalBadge> metaSet = wallet.getAllBadges();
                  LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());
            
                  assertEquals(6, metaSet.size());
            
                  Iterator<DigitalBadge> it = new TreeSet(metaSet).iterator();
                  DigitalBadge b1 = it.next();
                  assertEquals(new DigitalBadgeMetadata(1, 0,557), b1.getMetadata());
                  DigitalBadge b2 = it.next();
                  assertEquals(new DigitalBadgeMetadata(2,782, 906), b2.getMetadata());
                  DigitalBadge b3 = it.next();
                  assertEquals(new DigitalBadgeMetadata(4,49625, 557), b3.getMetadata());
            
                  assertEquals(0, wallet.getDeletingBadges().size());
                  assertEquals(1, wallet.getDeadLinesPositions().size());
                  assertEquals(2030, wallet.getDeadLinesPositions().iterator().next());
            
              }
          ```
     - [ ] Pour le mode Json
        ```java
        @Test
        void testGetMetadataWithDeletedRecords() throws IOException {
            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_delete.json");
            DigitalWallet wallet = dao.getWalletMetadata();
            Set<DigitalBadge> metaSet = wallet.getAllBadges();
            LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());

            assertEquals(2, metaSet.size());

            Iterator<DigitalBadge> it = new TreeSet(metaSet).iterator();
            DigitalBadge b1 = it.next();
            assertEquals(new DigitalBadgeMetadata(1, 0,557), b1.getMetadata());
            DigitalBadge b2 = it.next();
            assertEquals(new DigitalBadgeMetadata(2,919, 906), b2.getMetadata());

            assertThrows(NoSuchElementException.class, new Executable() {
                @Override
                public void execute() throws Throwable {
                    it.next();
                }
            });

            assertEquals(0, wallet.getDeletingBadges().size());
            assertEquals(1, wallet.getDeadLinesPositions().size());
            assertEquals(2303, wallet.getDeadLinesPositions().iterator().next());

            String mime1 = b1.getMetadata().getImageType();
            assertEquals(ManagedImages.valueOf(mime1.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.png.getMimeType());
            String mime2 = b2.getMetadata().getImageType();
            assertEquals(ManagedImages.valueOf(mime2.substring(mime1.lastIndexOf('/')+1)).getMimeType(), ManagedImages.png.getMimeType());

        }
       ```

## Impact sur la sérialisation 

```plantuml


@startuml

title __New Impacts on DB's Class Diagram__\n



  namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    interface fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.WalletSerializer {
        {abstract} + rollback()
    }
  }
namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    interface fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer {
        {abstract} + getSerializingStream()
        {abstract} + getSourceInputStream()
        {abstract} + serialize()
    }
  }

  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractWalletSerializer .up.|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.WalletSerializer
  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractWalletSerializer -up-|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer
fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer .up.|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer

  namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    abstract class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer {
        + serialize()
    }
  }


  namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    abstract class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractWalletSerializer {
        {abstract} + serialize()
    }
  }


  namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    namespace impl.db{
             namespace json {
               class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl {
              + JSONWalletSerializerDAImpl()
              + getSerializingStream()
              + getSourceInputStream()
              + rollback()
              + serialize()
          }
       }
    }
  }


  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl -up-|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractWalletSerializer


  namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    namespace impl.db {
        class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl {
            {static} - LOG : Logger
            + deserialize()
            + markAsDeleted()
            {static} + readLastLine()
      }
    }
  }

  

  namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    namespace impl.db {
        class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl {
            + WalletSerializerDirectAccessImpl()
            + getMetas()
            + getSerializingStream()
            + getSourceInputStream()
            + serialize()
      }
    }
  }
  

  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl .up.|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer
  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl -up-|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractWalletSerializer

namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    namespace impl.db {
        namespace json {
          class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl {
              {static} - LOG : Logger
              + deserialize()
              + markAsDeleted()
        }
      }
    }
  }
  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl .up.|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer
  namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    interface fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer {
        {abstract} + deserialize()
        {abstract} + markAsDeleted()
    }
  }

right footer
Ce diagramme résume les impacts sur le module badges-wallet, concernant la sérialisation
endfooter

@enduml




```

 - [ ] Ajouter au Sérialiseur de Badge une méthode permettant de marquer un badge comme supprimé. Nous voulons appliquer celà à toute base en accès direct, à savoir quel que soit le format, csv ou bien json. Donc nous allons appliquer une refactorisation afin d'être en concordance avec le diagramme classe ci-dessus.
   - [ ] Commencer par définir l'interface :
   ```java
   public interface WalletSerializer<S, M> extends ImageStreamingSerializer<S,M> {
       
       void rollback(S source, M media) throws IOException;
   
   }
   ```
   - [ ] Puis nous allons écrire une classe abstraite **AbstractWalletSerializer extends AbstractStreamingImageSerializer** ET implémentant cette interface, qui surcharge la méthode **serialize()** de sa super-classe sous forme de prototype abstrait, pour enfin forcer une surcharge au niveau de nos 2 implémentations cible. 

   ```java
   public abstract class AbstractWalletSerializer extends AbstractStreamingImageSerializer<DigitalBadge, WalletFrameMedia> implements  WalletSerializer<DigitalBadge, WalletFrameMedia> {
     @Override
     public abstract void serialize(DigitalBadge source, WalletFrameMedia media) throws IOException ;
   }
   ```
   - [ ] Et voici que, grâce à notre typage, nous imposons l'écriture de la méthode **rollback()** soit au niveau de **JSONWalletSerializerDAImpl** et **WalletSerializerDirectAccessImpl**, soit au niveau de **AbstractWalletSerializer** (un 2 en 1 est possible puisque nous utilisons exactement le même format de marquage).
     - Donc à vous de jouer...
   ```java
   @Override
   public void rollback(DigitalBadge source, WalletFrameMedia media) throws IOException {
      // TODO
   }
   ```
      
## Impact sur Les DAOs

 - [ ] Identifier les modifications nécessaires à apporter pour respecter le nouveau contrat ci-dessous:
   ```java
   public interface DirectAccessBadgeWalletDAO extends BadgeWalletDAO {
   
       DigitalWallet getWalletMetadata() throws IOException;
       
       void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException;
       void addBadge(DigitalBadge badge) throws IOException;
   
       void removeBadge(DigitalBadge badge) throws IOException;
   }
   ```
 - [ ] Veiller à ce que les différentes implémentations compilent ...
   - [ ] Nous ne souhaitons pas impacter le dé-sérialiseur, donc modifier simplement les DAOs **DirectAccessBadgeWalletDAOImpl** et **JSONBadgeWalletDAOImpl** pour qu'ils compilent toujours, et cela devrait fonctionner
   - [ ] Par contre si vous arrivez à ce point, vous avez conscience du développement réel à apporter dans chacun des 2 DAOs, soit au niveau de la méthode
     ```java
         @Override
         public void removeBadge(DigitalBadge badge) throws IOException {
           //TODO
         }
     ```
     
### Impact sur la couche de Service

 - [ ] Enfin nous allons passer à la phase de test, mais avant cela n'oubliez pas l'essentiel: l'impact implicite sur la couche service => Notre service Rest peut à présent faire appel à notre DAO amélioré au niveau de sa méthode de suppression vide...


## Tests

### Unitaires

- [ ] Ajoutez une nouvelle méthode de test, spécifiquement pour tester la suppression en tant que telle, pour chacun des Tests Unitaires:
  - [ ] DirectAccessBadgeWalletDAOTest
  - [ ] JSONBadgeWalletDAOTest
  - ... Evidemment une fois les tests écrits exécutez les puis rectifiez votre code jusqu'à respect de votre contrat de test :) ...
- [ ] Merci pour vos chefs-d'oeuvre...
  - ![](screenshots/All-Tests-5.png)
  - ![](screenshots/All-Tests-6.png)

### D'acceptance (de recette)

- [ ] Essayez à présent de supprimer un badge, en observant:
  - [ ] Les métadonnées avant suppression:
    - ![](screenshots/avant_sup.png)
  - [ ] Le comportement de l'API lors de la suppression:
    - ![](screenshots/supp.png)
  - [ ] Les métadonnées après le suppression:
    - ![](screenshots/apres_sup.png)
  
**FIN d'Exercice et de séance de cour n°7**

----

## Ressources supplémentaires

Les tests unitaires codés dans le correctif du présent exercice sont accessible dans le répertoire help:
 - [JSONBadgeWalletDAOTest](help/JSONBadgeWalletDAOTest.java)
 - [DirectAccessBadgeWalletDAOTest](help/DirectAccessBadgeWalletDAOTest.java)

Ils sont ici à titre informatif uniquement et ne représentent pas un idéal de solution. 

De même, les échantillons de tests csv et json s'y trouvent:
- [db_wallet_deleted](help/db_wallet_deleted.csv)
- [db_wallet_delete](help/db_wallet_delete.json)

Et encore une fois, rien ne vous oblige à faire exactement la même chose: ils sont ici à titre informatif uniquement et ne représentent pas un idéal de solution.


## Défragmentation  (REMIS à PLUS TARD => non noté pour le moment, mais sujet d'examen blanc potentiel)
  
 - [ ] Développer un traitement "batch" permettant de nettoyer le fichier de base de données Json suite à un certain nombre de suppressions
   - [ ] Il s'agit de lire le fichier de base json octet par octet en sautant les lignes supprimées (comme nous en connaissons les positions), et d'écrire cela dans la foulée vers un nouveau fichier, temporaire
   - [ ] En post-traitement il conviendra d'archiver la base originale, puis de renommer le fichier temporaire du nom du fichier original








