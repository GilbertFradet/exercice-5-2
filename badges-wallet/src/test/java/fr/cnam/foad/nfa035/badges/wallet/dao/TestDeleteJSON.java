package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.ManagedImages;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestDeleteJSON {
    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final Logger LOG = LogManager.getLogger(TestDeleteJSON.class);
    private static final File walletDatabase = new File(RESOURCES_PATH + "db_wallet_delete.json");

    @BeforeEach
    @Test
    public void init() throws IOException {
        if (walletDatabase.exists()) {
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
        try {

            JSONBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_delete.json");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2022-01-03");

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge1 = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge1);
            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            DigitalBadge badge2 = new DigitalBadge("NFA031", begin, end, null, image2);
            dao.addBadge(badge2);
            // 3ème Badge
            File image3 = new File(RESOURCES_PATH + "superman.jpg");
            DigitalBadge badge3 = new DigitalBadge("NFA035", begin, end, null, image3);
            dao.addBadge(badge3);
            dao.removeBadge(badge1);
           // dao.undoRemove(badge1);
            // dao.deleteBadge(badge2);

        } catch (Exception e) {

        }
    }

    /*@Test
    void testGetMetadataWithDeletedRecords() throws IOException, ArrayIndexOutOfBoundsException {
        System.out.println("hello");
        DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_delete.json");
        DigitalWallet wallet = new DigitalWallet(dao.getWalletMetadata());
        Set<DigitalBadge> metaSet = wallet.getAllBadges();
        System.out.println(metaSet.toString());
        ;
        LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());

        assertEquals(2, metaSet.size());

        Iterator<DigitalBadge> it = new TreeSet(metaSet).iterator();
        DigitalBadge b1 = it.next();
        assertEquals(new DigitalBadgeMetadata(1, 0, 557), b1.getMetadata());
        DigitalBadge b2 = it.next();
        assertEquals(new DigitalBadgeMetadata(2, 919, 906), b2.getMetadata());

        assertThrows(NoSuchElementException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                it.next();
            }
        });

        assertEquals(0, wallet.getDeletingBadges().size());
        assertEquals(1, wallet.getDeadLinesPositions().size());
        assertEquals(2303, wallet.getDeadLinesPositions().iterator().next());

        String mime1 = b1.getMetadata().getImageType();
        assertEquals(ManagedImages.valueOf(mime1.substring(mime1.lastIndexOf('/') + 1)).getMimeType(), ManagedImages.png.getMimeType());
        String mime2 = b2.getMetadata().getImageType();
        assertEquals(ManagedImages.valueOf(mime2.substring(mime1.lastIndexOf('/') + 1)).getMimeType(), ManagedImages.png.getMimeType());

    }*/
}