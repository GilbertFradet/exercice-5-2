package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static fr.cnam.foad.nfa035.badges.wallet.dao.JSONBadgeWalletDAOTest.walletDatabase;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestDeleteCsv {
    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final Logger LOG = LogManager.getLogger(TestDeleteCsv.class);
    private static final File walletDatabase = new File(RESOURCES_PATH + "db_wallet_deleted.csv");

    @BeforeEach
    @Test
    public void init() throws IOException {
        if (walletDatabase.exists()) {
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
        try {

            DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_deleted.csv");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2022-01-03");

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge1 = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge1);
            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            DigitalBadge badge2 = new DigitalBadge("NFA031", begin, end, null, image2);
            dao.addBadge(badge2);
            // 3ème Badge
            File image3 = new File(RESOURCES_PATH + "superman.jpg");
            DigitalBadge badge3 = new DigitalBadge("NFA035", begin, end, null, image3);
            dao.addBadge(badge3);
            dao.removeBadge(badge2);
           // dao.undoRemove(badge2);
           // dao.deleteBadge(badge2);

        } catch (Exception e) {

        }
    }

    @Test
    void testGetMetadataWithDeletedRecords() throws IOException {
        DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_deleted.csv");
        DigitalWallet wallet = new DigitalWallet(dao.getWalletMetadata());
        Set<DigitalBadge> metaSet = wallet.getAllBadges();
        LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());


        assertEquals(3, metaSet.size());

        Iterator<DigitalBadge> it = new LinkedHashSet(metaSet).iterator();
        DigitalBadge b1 = it.next();
        assertEquals(new DigitalBadgeMetadata(1, 0, 557), b1.getMetadata());
        DigitalBadge b2 = it.next();
        assertEquals(new DigitalBadgeMetadata(-1, 782, 906), b2.getMetadata());
        DigitalBadge b3 = it.next();
        System.out.println("b1" + b1.toString());
        assertEquals(new DigitalBadgeMetadata(3, 2030, 35664), b3.getMetadata());

        wallet.addDeletingBadge();
        assertEquals(1, wallet.getDeadLinesPositions().size());
        assertEquals(1, wallet.getDeletingBadges().size());

        assertEquals(782, wallet.getDeadLinesPositions().iterator().next());

    }
}
