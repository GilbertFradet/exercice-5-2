package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractWalletSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.WalletSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Set;
import java.util.TreeSet;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badges digitaux multiples
 * et PAR ACCES DIRECT, donc prenant en compte les métadonnées de chaque badges.
 */
@Component("directAccess")
public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOImpl.class);

    private final File walletDatabase;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public DirectAccessBadgeWalletDAOImpl(@Value("#{ systemProperties['db.path'] ?: 'badges-wallet/src/test/resources/db_wallet_indexed.csv'}") String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     * @hidden
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de préférence addBadge(DigitalBadge badge)
     */
    @Override
    public void addBadge(File image) throws IOException {
        LOG.error("Non supporté");
        throw new IOException("Méthode non supportée, utilisez plutôt addBadge(DigitalBadge badge)");
    }

    /**
     * Permet d'ajouter le badge au nouveau format de Wallet
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            ImageStreamingSerializer serializer = new WalletSerializerDirectAccessImpl(getWalletMetadata());
            serializer.serialize(badge, media);
        }
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de préférence getBadgeFromMetadata
     */
    @Override
    public void getBadge(OutputStream imageStream) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }


    /**
     * {@inheritDoc}
     *
     * @return List<DigitalBadgeMetadata>
     */
    @Override
    public Set<DigitalBadge> getWalletMetadata() throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            return (new MetadataDeserializerDatabaseImpl().deserialize(media));
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException {
        Set<DigitalBadge> metas = this.getWalletMetadata();
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            new WalletDeserializerDirectAccessImpl(imageStream, metas).deserialize(media, meta);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void removeBadge(DigitalBadge badge) throws IOException {
        MetadataDeserializerDatabaseImpl remover = new MetadataDeserializerDatabaseImpl();

        remover.markAsDeleted(walletDatabase, badge);//ou bien?
        // badge.makeDeadBadge();

    }

    /**
     * undoRemove
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void undoRemove(DigitalBadge badge) throws IOException {
        WalletSerializerDirectAccessImpl undoer = new WalletSerializerDirectAccessImpl();
        undoer.rollback(walletDatabase, badge);
    }

    /**
     * suppression définitive
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void deleteBadge(DigitalBadge badge) throws IOException {
        MetadataDeserializer remover = new MetadataDeserializerDatabaseImpl();
        remover.deleteBadge(walletDatabase, badge);
    }

}
