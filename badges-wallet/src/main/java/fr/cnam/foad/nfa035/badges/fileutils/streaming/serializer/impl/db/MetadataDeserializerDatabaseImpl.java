package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalWallet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {

    private static final Logger LOG = LogManager.getLogger(MetadataDeserializerDatabaseImpl.class);

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public Set<DigitalBadge> deserialize(WalletFrameMedia media) throws IOException {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        BufferedReader br = media.getEncodedImageReader(false);
        return br.lines()
                .map(
                        l -> l.split(";")
                )
                .filter(s -> s.length == 7)
                .map(
                        s -> {
                            Date begin = null, end = null;
                            try {
                                begin = format.parse(s[4]);
                            } catch (ParseException e) {
                                LOG.error("Problème de parsage, on considère la date Nulle", e);
                            }
                            try {
                                end = format.parse(s[5]);
                            } catch (ParseException e) {
                                LOG.error("Problème de parsage, on considère la date Nulle", e);
                            }
                            return new DigitalBadge(s[3], begin, end, new DigitalBadgeMetadata(Integer.parseInt(s[0]), Long.parseLong(s[1]), Long.parseLong(s[2])), null);
                        }
                )
                .collect(Collectors.toSet());
    }


    @Override
    public void markAsDeleted(File f, DigitalBadge badge) throws IOException {
        String line = "";
        try (Stream<String> lines = Files.lines(f.toPath())) {
            line = lines.skip(badge.getMetadata().getBadgeId() - 1).findFirst().get();
        }
        String Id = line.split(";")[0];
        String pos = line.split(";")[1];
        String size = line.split(";")[2];
        String serial = line.split(";")[3];
        String begin = line.split(";")[4];
        String end = line.split(";")[5];
        String oldMetas = Id + ";" + pos + ";" + size + ";" + serial + ";" + begin + ";" + end;
        String newMetas = "-1;" + pos + ";" + size + ";" + serial + ";" + begin + ";" + end;
        Scanner sc = new Scanner(f);
        StringBuffer buffer = new StringBuffer();
        while (sc.hasNextLine()) {
            buffer.append(sc.nextLine() + System.lineSeparator());
        }
        String fileContents = buffer.toString();
        sc.close();
        fileContents = fileContents.replaceAll(oldMetas, newMetas);
        FileWriter writer = new FileWriter(f.getPath());
        writer.append(fileContents);
        writer.flush();

    }

    /**
     * Supprime définitivement le badge après un markasDeleted
     *
     * @param f
     * @param badge
     * @throws IOException
     */
    @Override
    public void deleteBadge(File f, DigitalBadge badge) throws IOException {
        Scanner sc = new Scanner(f);
        StringBuffer buffer = new StringBuffer();
        long count = 0;
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            count++;
            if (Integer.parseInt(s.split(";")[0]) == -1 && Long.parseLong(s.split(";")[1]) == badge.getMetadata().getWalletPosition()) {
                s="";
            }
            buffer.append(s + System.lineSeparator());
        }
        String fileContents = buffer.toString();
        sc.close();
        FileWriter writer = new FileWriter(f.getPath());
        writer.append(fileContents);
        writer.flush();
    }

   /* *//**
     * {@inheritDoc}
     *
     * @param f
     * @param badge
     * @throws IOException
     *//*
    @Override
    public void rollback(File f, DigitalBadge badge) throws IOException {
        Scanner sc = new Scanner(f);
        String line = "";
        StringBuffer buffer = new StringBuffer();
        long count = 0;
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            count++;
            if (Integer.parseInt(s.split(";")[0])==-1 && Long.parseLong(s.split(";")[1])==badge.getMetadata().getWalletPosition()) {
                String s2 = s.substring(s.split(";")[0].length() + 1);
                s=Long.toString(count)+";"+s2;
            }
            buffer.append(s + System.lineSeparator());
        }
        String fileContents = buffer.toString();
        sc.close();
        FileWriter writer = new FileWriter(f.getPath());
        writer.append(fileContents);
        writer.flush();
    }*/

    /**
     * Peremet de lire la dernière ligne d'un fichier en accès direct
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for (long seek = length - 1; seek >= 0; --seek) {
            file.seek(seek);
            char c = (char) file.read();
            if (c != '\n') {
                builder.append(c);
            } else {
                break;
            }
        }
        return builder.reverse().toString();
    }

    /**
     * modif
     * @param file
     * @return
     * @throws IOException
     */
   /* public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }
*/
}
