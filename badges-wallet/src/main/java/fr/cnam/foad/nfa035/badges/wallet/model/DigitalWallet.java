package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class DigitalWallet {
    Set<DigitalBadge> allBadges = new TreeSet<>();
    Set<Long> deadLinesPositions = new TreeSet<>();
    Set<DigitalBadge> deletingBadges = new TreeSet<>();

    /**
     * Constructeur
     *
     * @param allBadges
     */
    public DigitalWallet(Set<DigitalBadge> allBadges) {
        this.allBadges = allBadges;
    }

    /**
     * fonction d'ajout des badges à détruire au Set deletingBadges
     */
    public void addDeletingBadge() throws ArrayIndexOutOfBoundsException {
        Iterator iter = allBadges.iterator();
        while (iter.hasNext()) {
            DigitalBadge badge = (DigitalBadge) iter.next();
            if (badge.getMetadata().getBadgeId() == -1) {
                deletingBadges.add(badge);
                setDeadLinePositions(badge.getMetadata().getWalletPosition());
            }
        }
    }


    /**
     * commitBadgeDeletion (je ne sais pas comment va marcher cette méthode)
     */
    public void commitBadgeDeletion() {
        //TODO
    }


    /**
     * fonction d'ajout de la position au Set  deadLinesPositions
     *
     * @param line
     */
    public void setDeadLinePositions(long line) throws ArrayIndexOutOfBoundsException {
        deadLinesPositions.add(line);
    }


    /**
     * Setters&getters
     *
     * @return
     */
    public Set<DigitalBadge> getAllBadges() throws ArrayIndexOutOfBoundsException {
        return allBadges;
    }

    public void setAllBadges(TreeSet<DigitalBadge> allBadges) {
        this.allBadges = allBadges;
    }

    public Set<Long> getDeadLinesPositions() throws ArrayIndexOutOfBoundsException {
        return deadLinesPositions;
    }

    public void setDeadLinesPositions(TreeSet<Long> deadLinesPositions) {
        this.deadLinesPositions = deadLinesPositions;
    }

    public Set<DigitalBadge> getDeletingBadges() throws ArrayIndexOutOfBoundsException {
        return deletingBadges;
    }

    public void setDeletingBadges(TreeSet<DigitalBadge> deletingBadges) throws ArrayIndexOutOfBoundsException {
        this.deletingBadges = deletingBadges;
    }
}
