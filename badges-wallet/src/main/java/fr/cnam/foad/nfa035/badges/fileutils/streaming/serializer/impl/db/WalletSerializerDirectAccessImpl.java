package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.proxy.Base64OutputStreamProxy;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.Set;

/**
 * Implémentation Base64 de sérialiseur d'image, basée sur des flux.
 * TODO
 */
public class WalletSerializerDirectAccessImpl
        extends AbstractStreamingImageSerializer<DigitalBadge, WalletFrameMedia> {

    Set<DigitalBadge> metas;

    public WalletSerializerDirectAccessImpl(Set<DigitalBadge> metas) {
        this.metas = metas;
    }
    public WalletSerializerDirectAccessImpl() {

    }

    /**
     * {@inheritDoc}
     *
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(DigitalBadge source) throws IOException {
        return new FileInputStream(source.getBadge());
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(WalletFrameMedia media) throws IOException {
        return new Base64OutputStreamProxy(new Base64OutputStream(media.getEncodedImageOutput(),true,0,null));
    }


    @Override
    public final void serialize(DigitalBadge source, WalletFrameMedia media) throws IOException {
        RandomAccessFile random = media.getChannel();
        if (metas.contains(source)){
            throw new IOException("Badge déjà présent dans le Wallet");
        }
        long size = Files.size(source.getBadge().toPath());
        try(OutputStream os = media.getEncodedImageOutput()) {
            long numberOfLines = media.getNumberOfLines();
            long newPos = Long.parseLong(MetadataDeserializerDatabaseImpl.readLastLine(random).split(";")[1]);
            random.seek(random.length());
            DigitalBadgeMetadata meta = new DigitalBadgeMetadata((int)numberOfLines + 1, newPos,size);
            source.setMetadata(meta);
            PrintWriter writer = new PrintWriter(os, true, StandardCharsets.UTF_8);
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            writer.printf("%1$d;%2$s;%3$s;%4$s;", size, source.getSerial(), format.format(source.getBegin()), format.format(source.getEnd()));
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                eos.flush();
            }
            writer.printf("\n");
            writer.printf("%1$d;%2$d;",numberOfLines + 2, random.getFilePointer());
        }
        media.incrementLines();
    }
    /**
     * {@inheritDoc}
     *
     * @param f
     * @param badge
     * @throws IOException
     */
    @Override
    public void rollback(File f, DigitalBadge badge) throws IOException {
        Scanner sc = new Scanner(f);
        StringBuffer buffer = new StringBuffer();
        long count = 0;
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            count++;
            if (Integer.parseInt(s.split(";")[0])==-1 && Long.parseLong(s.split(";")[1])==badge.getMetadata().getWalletPosition()) {
                String s2 = s.substring(s.split(";")[0].length() + 1);
                s=Long.toString(count)+";"+s2;
            }
            buffer.append(s + System.lineSeparator());
        }
        String fileContents = buffer.toString();
        sc.close();
        FileWriter writer = new FileWriter(f.getPath());
        writer.append(fileContents);
        writer.flush();
    }

}
