package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.proxy.Base64OutputStreamProxy;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.Set;

/**
 * Implémentation Base64 de sérialiseur d'image, basée sur des flux.
 * TODO
 */
public class JSONWalletSerializerDAImpl
        extends AbstractStreamingImageSerializer<DigitalBadge, WalletFrameMedia> {

    Set<DigitalBadge> metas;

    public JSONWalletSerializerDAImpl() {

    }

    public JSONWalletSerializerDAImpl(Set<DigitalBadge> metas) {
        this.metas = metas;
    }

    /**
     * {@inheritDoc}
     *
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(DigitalBadge source) throws IOException {
        return new FileInputStream(source.getBadge());
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(WalletFrameMedia media) throws IOException {
        return new Base64OutputStreamProxy(new Base64OutputStream(media.getEncodedImageOutput(), true, 0, null));
    }


    @Override
    public final void serialize(DigitalBadge source, WalletFrameMedia media) throws IOException {
        if (metas.contains(source)) {
            throw new IOException("Badge déjà présent dans le Wallet");
        }
        long size = Files.size(source.getBadge().toPath());
        try (OutputStream os = media.getEncodedImageOutput()) {

            JsonFactory jsonFactory = new JsonFactory();
            jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
            ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

            RandomAccessFile directFile = media.getChannel();
            String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(directFile);
            DigitalBadge amorce = objectMapper.readValue(lastLine.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);
            DigitalBadgeMetadata meta = new DigitalBadgeMetadata(amorce.getMetadata().getBadgeId(), amorce.getMetadata().getWalletPosition(), size);
            source.setMetadata(meta);
            // Gestion du mimeType
            String fileName = source.getBadge().getName();
            ManagedImages mimeType = ManagedImages.valueOf(fileName.substring(fileName.lastIndexOf('.') + 1));
            meta.setImageType(mimeType.getMimeType());
            directFile.seek(meta.getWalletPosition());

            PrintWriter writer = new PrintWriter(os, true, StandardCharsets.UTF_8);
            if (meta.getBadgeId() == 1) {
                writer.printf("[");
            }
            writer.printf("[{\"badge\":");
            objectMapper.writeValue(os, source);
            writer.printf("},{\"payload\":\"");

            try (OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                eos.flush();
            }

            writer.printf("\"}],\n");

            long newPosition = directFile.getFilePointer();


            DigitalBadge nouvelleAmorce = new DigitalBadge(null, null, null, new DigitalBadgeMetadata(meta.getBadgeId() + 1, newPosition, -1), null);
            writer.printf("[{\"badge\":");
            objectMapper.writeValue(os, nouvelleAmorce);
            writer.printf("},{\"payload\":null}]]");
        }
        media.incrementLines();
    }

    /**
     * {@inheritDoc}
     *
     * @param f
     * @param badge
     * @throws IOException
     */
    @Override
    public void rollback(File f, DigitalBadge badge) throws IOException {
        Scanner sc = new Scanner(f);
        StringBuffer buffer = new StringBuffer();
        long count = 0;
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            count++;
            int checkIt = Integer.parseInt(s.split("\\[\\{\"badge\":\\{\"metadata\":\\{\"badgeId\":")[1].split(",\"walletPosition\":")[0]);
            System.out.println("checkIt" + checkIt);
            String toCheck = s.split("\\{\"payload\":\"")[0].split("\"walletPosition\":")[1];
            String check = "" + badge.getMetadata().getWalletPosition() + ",\"imageSize\":" + badge.getMetadata().getImageSize() + ",\"imageType\":\"" + badge.getMetadata().getImageType() + "\"},\"serial\":\"" + badge.getSerial() + "\",\"begin\":" + badge.getBegin().getTime() + ",\"end\":" + badge.getEnd().getTime() + "}},";
            System.out.println("Check: " + check);
            System.out.println("ToChk: " + toCheck);
            //  System.out.println("String: " + s);
            System.out.println(count);
            // String string=s.split("\\[\\{\"badge\":\\{\"metadata\":\\{\"badgeId\":")[1];
            // System.out.println("String: "+string);
            if (checkIt == -1 && toCheck.equals(check)) {
                s="[{\"badge\":{\"metadata\":{\"badgeId\":"+count+",\"walletPosition\":"+s.split(",\"walletPosition\":")[1];
               if (count==1)
                   s="["+s;
                System.out.println("s: "+s);
            }
            buffer.append(s + System.lineSeparator());
        }
        String fileContents = buffer.toString();
        sc.close();
        FileWriter writer = new FileWriter(f.getPath());
        writer.append(fileContents);
        writer.flush();
    }

}

