package fr.cnam.foad.nfa035.badges.wallet.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.File;
import java.util.Date;

/**
 * POJO model représentant le Badge Digital
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DigitalBadge implements Comparable<DigitalBadge> {

    private DigitalBadgeMetadata metadata;

    @JsonIgnore
    private transient File badge;

    private String serial;
    private Date begin;
    private Date end;

    /**
     * Constructeur complet
     *
     * @param metadata
     * @param badge
     * @param serial
     * @param begin
     * @param end
     */
    public DigitalBadge(String serial, Date begin, Date end, DigitalBadgeMetadata metadata, File badge) {
        this.metadata = metadata;
        this.badge = badge;
        this.serial = serial;
        this.begin = begin;
        this.end = end;
    }

    /**
     * Constructeur minimal
     *
     * @param serial
     * @param begin
     * @param end
     */
    public DigitalBadge(String serial, Date begin, Date end) {
        this.serial = serial;
        this.begin = begin;
        this.end = end;
    }

    /**
     * COnstructeur par défaut
     */
    public DigitalBadge() {
    }

    /**
     * Getter des métadonnées du badge
     *
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées du badge
     *
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     *
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter du badge (Fichier image)
     *
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * Setter du code de série du badge
     *
     * @return String
     */
    public String getSerial() {
        return serial;
    }

    /**
     * Getter du code de série du badge
     *
     * @param serial
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     *positionne l' Id à -1
     */
    public void makeDeadBadge() {
        this.getMetadata().setBadgeId(-1);
    }

    /**
     * Setter de la date d'obtention du badge
     *
     * @return Date
     */
    public Date getBegin() {
        return begin;
    }

    /**
     * Getter de la date d'obtention du badge
     *
     * @param begin
     */
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    /**
     * Setter de la date de péremption du badge, ou null si illimité
     *
     * @return Date
     */
    public Date getEnd() {
        return end;
    }

    /**
     * Setter de la date de péremption du badge
     *
     * @param end
     */
    public void setEnd(Date end) {
        this.end = end;
    }

    /**
     * {@inheritDoc}
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return new EqualsBuilder().append(serial, that.serial).append(end, that.end).isEquals();
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(serial).append(end).toHashCode();
    }

    /**
     * {@inheritDoc}
     *
     * @return String
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                ", serial='" + serial + '\'' +
                ", begin=" + begin +
                ", end=" + end +
                '}';
    }


    /**
     * {@inheritDoc}
     *
     * @param o
     * @return int
     */
    @Override
    public int compareTo(DigitalBadge o) {
        return metadata.compareTo(o.getMetadata());
    }
}
