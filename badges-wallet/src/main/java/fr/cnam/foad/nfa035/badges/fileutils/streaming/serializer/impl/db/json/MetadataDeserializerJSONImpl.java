package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class MetadataDeserializerJSONImpl extends MetadataDeserializerDatabaseImpl implements MetadataDeserializer {

    private static final Logger LOG = LogManager.getLogger(MetadataDeserializerJSONImpl.class);

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public Set<DigitalBadge> deserialize(WalletFrameMedia media) throws IOException, ArrayIndexOutOfBoundsException {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        BufferedReader br = media.getEncodedImageReader(false);

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);


        return br.lines()
                .map(
                        l -> {
                            try {
                                String badge = l.split(",\\{\"payload")[0].split(".*badge\":")[1];
                                DigitalBadge digitalBadge = objectMapper.readValue(badge, DigitalBadge.class);
                                return digitalBadge.getMetadata().getImageSize() == -1 ? null : digitalBadge;
                            } catch (IOException ioException) {
                                LOG.error("Problème de parsage JSON, on considère l'enregistrement Nul", ioException);
                            }
                            return null;
                        }
                ).filter(x -> x != null).collect(Collectors.toSet());
    }

    @Override
    public void markAsDeleted(File f, DigitalBadge badge) throws IOException {
        String line = "";
        try (Stream<String> lines = Files.lines(f.toPath())) {
            line = lines.skip(badge.getMetadata().getBadgeId() - 1).findFirst().get();
        }
        String metas = "badgeId\":-1,\"walletPosition\":";
        String metas2 = line.split("walletPosition\":")[1].split("\\}\\},\\{\"payload\":")[0];
        metas = metas + metas2;
        String oldMetas = line.split("\\{\"metadata\":\\{\"")[1].split("\\}\\},\\{\"payload\":")[0];
        Scanner sc = new Scanner(f);
        StringBuffer buffer = new StringBuffer();
        while (sc.hasNextLine()) {
            buffer.append(sc.nextLine() + System.lineSeparator());
        }
        String fileContents = buffer.toString();
        sc.close();
        fileContents = fileContents.replaceAll(oldMetas, metas);
        FileWriter writer = new FileWriter(f.getPath());
        writer.append(fileContents);
        writer.flush();

    }

    /**
     * Supprime définitivement le badge après un markAsDeleted
     *
     * @param f
     * @param badge
     * @throws IOException
     */
    @Override
    public void deleteBadge(File f, DigitalBadge badge) throws IOException, ArrayIndexOutOfBoundsException {
        Scanner sc = new Scanner(f);
        StringBuffer buffer = new StringBuffer();
        long count = 0;
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            count++;
            int condition1 = Integer.parseInt(s.split("\"badgeId\":")[1].split(",\"walletPosition\":")[0]);
            String condition2 = s.split(",\"walletPosition\":")[1].split("\\}\\},\\{\"payload\":\"")[0];
            String condition2check = "" + badge.getMetadata().getWalletPosition() + ",\"imageSize\":" + badge.getMetadata().getImageSize() +
                    ",\"imageType\":\"" + badge.getMetadata().getImageType() + "\"},\"serial\":\"" + badge.getSerial() + "\",\"begin\":" + badge.getBegin().getTime() + ",\"end\":" + badge.getEnd().getTime();
            if (condition1 == -1 && condition2.equals(condition2check)) {
                if (count == 1) {
                    s = "[";
                } else
                    s = "";
            }

            buffer.append(s + System.lineSeparator());
        }
        String fileContents = buffer.toString();
        sc.close();
        FileWriter writer = new FileWriter(f.getPath());
        writer.append(fileContents);
        writer.flush();
    }


}
