package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import java.io.File;
import java.io.IOException;

public interface WalletSerializer<S, M>{
    /**
     * permet d'annuler la suppression avant qu'il ne soit trop tard (avant deleteBadge)
     *
     * @param f
     * @param badge
     * @throws IOException
     */
    void rollback(File f, DigitalBadge badge) throws IOException;
   // void rollback(S source, M media) throws IOException;
}
